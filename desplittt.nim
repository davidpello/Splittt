import os
import std/math
import std/strutils
import arc4
import splittt

when isMainModule:
    # check arguments
    if paramCount() < 3:
        echo "Usage:"
        echo "desplittt <input file> <output file> <key>"
        echo "\t <input file> is the first of the splitted files."
        echo "\t <key> must be five to sixteen characters long"
        quit(1)

    # check key
    var key_str = paramStr(3)
    var key_len = key_str.len

    if key_len < 5:
        echo "Key too short!"
        quit(1)
    if key_len > 16:
        echo "Key too long!"
        quit(1)

    echo "Key lenght: ", key_len

    
    # test file
    var fp: File
    if not open(fp, paramStr(1), fmRead):
      echo "Error opening file"
      quit(1)

    # get data from file
    var header: Header
    discard readBuffer(fp, addr(header), sizeof(header))
    close(fp)

    echo "Number of files: ", header.count
    echo "Size of splits: ", header.split_size

    # digits
    var digits = int(log10(float32(header.count))) + 1
    echo "Digits: ", digits

    # read the files
    var fdname: string
    var box: array[256, uint8]
    var buffer = newSeq[uint8](header.split_size)
    var decbuffer = newSeq[uint8](header.split_size)

    var fd: File
    if not open(fd, paramStr(2), fmWrite):
      echo "Error opening output file"
      quit(1)

    for i in 0 ..< header.count:
      #var fd: File
      fdname = intToStr(int(i), digits) & ".sp"
      echo "Processing: ", fdname
      
      # read data
      if not open(fp, fdname, fmRead):
        echo "Error opening file ", fdname
        quit(1)
      
      # read header
      discard readBuffer(fp, addr(header), sizeof(header))

      # check header
      if header.check != check_calc(header.split_size, header.count, header.num):
        echo "Wrong checksum"
        quit(1)

      # read chunk
      var bytes_read = readBytes(fp, buffer, 0, header.split_size)
      if bytes_read != int(header.split_size):
        echo "Incorrect file size"
        quit(1)

      # decode it
      arc4_init(box, toOpenArrayByte(@key_str, 0, key_len-1))
      arc4_parse(box, buffer, decbuffer)

      # write data
      discard writeBytes(fd, decbuffer, 0, header.bytes)
      close(fp)

    close(fd)

