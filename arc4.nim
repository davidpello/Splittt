
proc arc4_init*(box: var openArray[uint8], key: openArray[uint8]) =
    var temp: array[256, uint8]
    
    # init boxes
    for i in countup(0, 255):
        box[i] = uint8(i)
        temp[i] = key[i mod len(key)]

    # prepare box
    var j = 0
    for i in countup(0, 255):
        j = (j + int(box[i]) + int(temp[i])) mod 256
        var t: uint8 = box[i]
        box[i] = box[j]
        box[j] = t

    # clear temp memory (contais key)
    for i in countup(0, 255):
        temp[i] = 0


proc arc4_parse*(box: var openArray[uint8], data: openArray[uint8], parsed_data: var openArray[uint8]) = 
    var i = 0
    var j = 0

    for count in countup(0, len(data)-1):
        # generate pseudo random byte
        i = (i+1) mod 256
        j = (j+int(box[i])) mod 256
        var temp: uint8 = box[i]
        box[i] = box[j]
        box[j] = temp
        var t = (box[i] + box[j]) mod 256
        var k: uint8 = box[t]

        # encode or decode (xor)
        parsed_data[count] = data[count] xor k
