import os
import std/math
import std/strutils
import arc4


type
  Header*{.packed.} = object
    num*: uint32
    count*: uint32
    split_size*: uint32
    bytes*: uint32
    check*: uint32

# simple calculation for testing the files
proc check_calc*(size, count, num: uint32): uint32 =
  result = (size xor count) + num

when isMainModule:
    # check arguments
    if paramCount() < 3:
        echo "Usage:"
        echo "splittt <file> <size> <key>"
        echo "\t <size> must be power of two (4, 8, 16, 32...)"
        echo "\t <key> must be five to sixteen characters long"
        quit(1)

    # check size
    var split_size = parseInt(paramStr(2))

    if split_size < 2:
        echo "Size must be bigger than 2"
        quit(1)

    if ceil(log2(float32(split_size))) != floor(log2(float32(split_size))):
        echo "Size must be power of two (4, 8, 16, 32...)"
        quit(1)

    echo "Split size: ", split_size

    # check key
    var key_str = paramStr(3)
    var key_len = key_str.len

    if key_len < 5:
        echo "Key too short!"
        quit(1)
    if key_len > 16:
        echo "Key too long!"
        quit(1)

    echo "Key lenght: ", key_len

    
    # test file
    var fp: File
    if not open(fp, paramStr(1), fmRead):
      echo "Error opening file"
      quit(1)

    # get size
    var size = getFileSize(fp)
    echo "File size: ", size

    var chunks = int(size div split_size)
    if chunks mod split_size > 0:
      chunks += 1

    echo "Chunks: ", chunks

    # digits
    var digits = int(log10(float32(chunks))) + 1
    echo "Digits: ", digits

    # write the files
    var fdname: string
    var box: array[256, uint8]
    var buffer = newSeq[uint8](split_size)
    var encbuffer = newSeq[uint8](split_size)

    for i in 0 ..< chunks:
      #var fd: File
      fdname = intToStr(i, digits) & ".sp"
      echo "Processing: ", fdname
      
      # read chunk
      var bytes_read = readBytes(fp, buffer, 0, split_size)

      # write data
      var fd: File
      if not open(fd, fdname, fmWrite):
        echo "Error opening file ", fdname
        quit(1)
      
      #header
      var header: Header
      header.num = uint32(i)
      header.count = uint32(chunks)
      header.split_size = uint32(split_size)
      header.bytes = uint32(bytes_read)
      header.check = check_calc(header.split_size, header.count, header.num)
      discard writeBuffer(fd, addr(header), sizeof(header))

      arc4_init(box, toOpenArrayByte(@key_str, 0, key_len-1))

      arc4_parse(box, buffer, encbuffer)

      # data
      discard writeBytes(fd, encbuffer, 0, split_size)
      close(fd)

    close(fp)




