# SPLITTT

splits a file in several smaller files of specified lenght, encoding each one using ARCFOUR (rc4) cipher with a variable lenght (40-128bit) key. 

Adds a simple header to the output files to help with the reassembly.


## Build

> $ nim c splittt.nim
> 
> $ nim c desplitt.nim

## Usage

> $ ./splittt <input_file> <split_size> <asc_key>

Splits the input file in several files, each of size **split_size**, and encodes each one using the provided ascii key.

Each output file will be numbered and have a .sp extension.

> $ ./desplittt <input_file> <output_file> <asc_key>

Tries to reassemble and decrypt the original file using the splitted files. The input file can be any of the splitted files. All the splitted files need to be present in the current directory. The reassembled file will be put in **output_file**





